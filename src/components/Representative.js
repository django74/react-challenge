import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Table } from 'reactstrap';

class Representative extends Component {
  render() {
    const repInfo = this.props.repInfo;
    return (
      <div>
        <Table className="rep-table">
          <tbody>
          <tr>
            <th scope="row">First Name</th>
            <td>{repInfo.first_name}</td>
          </tr>
          <tr>
            <th scope="row">Last Name</th>
            <td>{repInfo.last_name}</td>
          </tr>
          <tr>
            <th scope="row">Personal URL</th>
            <td>{repInfo.personal_url}</td>
          </tr>
          <tr>
            <th scope="row">District</th>
            <td>{repInfo.district_name}</td>
          </tr>
          <tr>
            <th scope="row">Party Name</th>
            <td>{repInfo.party_name}</td>
          </tr>
          <tr>
            <th scope="row">Email</th>
            <td>{repInfo.email}</td>
          </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}

Representative.propTypes = {
  repInfo: PropTypes.object,
};

export default Representative;