import React, {Component} from 'react';
import {Button} from '@blueprintjs/core';
import fetchJsonp from "fetch-jsonp";
import PropTypes from 'prop-types';

class PostalInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postalCode: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.getRepresentative = this.getRepresentative.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  /**
   * GET request to API and set in parent state MP object
   */
  getRepresentative() {
      let scope = this;
      fetchJsonp('https://represent.opennorth.ca/postcodes/' + this.state.postalCode)
      .then(function(response) {
        return response.json()
      }).then(function(json) {
        scope.props.setRepresentative(scope.getMP(json));
      }).catch(function(ex) {
        console.log('parsing failed', ex)
      })
  }

  /**
   * Parse out and return MP object information
   * @param representativeData - returned rep object
   * @returns object containing MP information
   */
  getMP(representativeData) {
    let result = '';
    representativeData['representatives_centroid'].forEach((elem) => {
      if (elem.elected_office === 'MP') {
        result = elem;
      }
    });
    return result;
  }

  /**
   * Save typed in postal code (removes spaces and converts to uppercase for
   * convenience in case user doesn't type in all upper or has space
   * @param e
   */
  handleChange(e) {
    this.setState({postalCode: e.target.value.replace(/\s/g,'').toUpperCase()})
  }

  handleKeyPress(e) {
    if (e.key === 'Enter') this.getRepresentative();
  }

  render() {
    return (
      <div>
        <h3 style={{'marginTop': '20px'}}>
          Enter Postal Code to find your MP!
        </h3>
        <input
          className="pt-input"
          type="search"
          placeholder="Postal Code"
          dir="auto"
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
        />
        <Button
                className="find-button"
                intent="Primary"
                icon="search"
                text="Find"
                onClick={this.getRepresentative}
        />
      </div>
    );
  }
}

PostalInput.propTypes = {
  setRepresentative: PropTypes.func,
};

export default PostalInput;