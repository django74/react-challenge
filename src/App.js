import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Representative from './components/Representative';
import PostalInput from './components/PostalInput';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      repInfo: {},
    };
    this.setRepresentative = this.setRepresentative.bind(this);
  }

  setRepresentative(repInfo) {
    this.setState({repInfo: repInfo})
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Find your MP</h1>
        </header>
        <div className="container">
          <div className="box">
            <PostalInput setRepresentative={this.setRepresentative}/>
            <Representative repInfo={this.state.repInfo}/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
